package com.kalakutta.web.initialData;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.kalakutta.service.dto.RoleTypeDTO;

public class APIConsumer {
	private static final String BASE_URI = "http://localhost:19000/rest";

	private static final String CREATE_ROLE_TYPE_URL = "/role/type/create";

	RestTemplate restTemplate = new RestTemplate();

	@Test
	public void test() {
		
	}

	@Test
	public void createRoleType() {
		RoleTypeDTO roleType = new RoleTypeDTO();
		roleType.setDescription("Soldier role type");
		roleType.setName("Soldier");

		RoleTypeDTO resultRoleTypeDTO = restTemplate.postForObject(BASE_URI + CREATE_ROLE_TYPE_URL, roleType,
				RoleTypeDTO.class);

		assertNotNull(resultRoleTypeDTO);

	}

}
