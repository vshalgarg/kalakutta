package com.kalakutta.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kalakutta.exception.RestException;
import com.kalakutta.service.RoleTypeService;
import com.kalakutta.service.dto.RoleFieldDTO;
import com.kalakutta.service.dto.RoleTypeDTO;
import com.kalakutta.service.exception.ServiceException;

/*
 * @author Vishal Garg
 */
@RestController
@RequestMapping("/role/type")
public class RoleTypeController {

	@Autowired
	private RoleTypeService roleTypeService;

	@PostMapping(value = "/create")
	public @Valid RoleTypeDTO createRoleType(@Valid @RequestBody RoleTypeDTO roleTypeDTO) throws RestException {
		// TODO replace with logger
		System.out.println(roleTypeDTO);

		try {
			roleTypeDTO = roleTypeService.create(roleTypeDTO);
		} catch (ServiceException e) {
			throw new RestException(e.getMessage(), e);
		}

		return roleTypeDTO;
	}

	@PostMapping(value = "/{roleTypeId}/add/field")
	public @Valid RoleTypeDTO addFieldToRole(@PathVariable(name = "roleTypeId") String roleTypeId,
			@Valid @RequestBody RoleFieldDTO roleFieldDTO) throws RestException {
		// TODO replace with logger
		System.out.println(roleFieldDTO);
		try {
			RoleTypeDTO roleTypeDTO = roleTypeService.addFieldToRole(roleTypeId, roleFieldDTO);
			return roleTypeDTO;
		} catch (ServiceException e) {
			throw new RestException(e.getMessage(), e);
		}
	}

	@PostMapping(value = "/{roleTypeId}/add/all/fields")
	public @Valid RoleTypeDTO addAllFieldsToRole(@PathVariable(name = "roleTypeId") String roleTypeId,
			@Valid @RequestBody List<RoleFieldDTO> roleFieldsDTO) throws RestException {
		// TODO replace with logger
		System.out.println(roleFieldsDTO);
		try {
			RoleTypeDTO roleTypeDTO = roleTypeService.addFieldsToRole(roleTypeId, roleFieldsDTO);
			return roleTypeDTO;
		} catch (ServiceException e) {
			throw new RestException(e.getMessage(), e);
		}
	}
	
//	@GetMapping(value="/get/all")
//	public @Valid List<RoleTypeDTO> getAllRoleTypes() throws RestException{
//		List<RoleTypeDTO> roleTypeDto;
//		try {
//			roleTypeDto = roleTypeService.getAllRoleTypes();
//		} catch (ServiceException e) {
//			throw new RestException(e.getMessage(), e);
//		}
//		return roleTypeDto;
//	}
}
