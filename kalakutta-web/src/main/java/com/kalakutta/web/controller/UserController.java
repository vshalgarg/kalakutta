package com.kalakutta.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kalakutta.exception.RestException;
import com.kalakutta.service.UserService;
import com.kalakutta.service.dto.UserDTO;
import com.kalakutta.service.exception.ServiceException;

@RestController
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(value = "/create")
	public @Valid UserDTO createNewUser(@Valid @RequestBody UserDTO dto) throws RestException {
		// TODO replace with logger
		System.out.println(dto);

		try {
			dto = userService.createUser(dto);
		} catch (ServiceException e) {
			throw new RestException(e.getMessage());
		}

		return dto;
	}

}
