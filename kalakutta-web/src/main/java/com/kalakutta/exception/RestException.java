package com.kalakutta.exception;

public class RestException extends Exception{
	private static final long serialVersionUID = -2152058777337556509L;

	public RestException() {
		super();
	}
	
	public RestException(Throwable e) {
		super(e);
	}
	
	public RestException(String msg, Throwable e) {
		super(msg,e);
	}
	
	public RestException(String msg) {
		super(msg);
	}
}
