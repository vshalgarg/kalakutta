package com.kalakutta.common.constants;

public interface Constants {
	String DATE_FORMAT = "dd-MM-yyyy";
	String DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
}
