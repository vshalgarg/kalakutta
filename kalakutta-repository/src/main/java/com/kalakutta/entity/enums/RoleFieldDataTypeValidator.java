package com.kalakutta.entity.enums;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.regex.Pattern;

import com.kalakutta.common.constants.Constants;

/**
 * @author Vishal Garg
 *
 */
public enum RoleFieldDataTypeValidator {
	MIN_LENGTH("java.lang.Integer") {
		/*
		 * value should have minimum length as validation
		 */
		@SuppressWarnings("rawtypes")
		@Override
		public boolean validate(Object value, Object validation) {
			Integer validationValue = Integer.valueOf(validation.toString());
			int valueSize;
			if (value instanceof String) {
				valueSize = value.toString().length();
			} else if (value instanceof Collection) {
				valueSize = ((Collection) value).size();
			} else {
				return true;
			}
			return valueSize >= validationValue.intValue();
		}
	},
	MAX_LENGTH("java.lang.Integer") {
		/*
		 * value should have maximum length as validation
		 */
		@SuppressWarnings("rawtypes")
		@Override
		public boolean validate(Object value, Object validation) {
			Integer validationValue = Integer.valueOf(validation.toString());
			int valueSize;
			if (value instanceof String) {
				valueSize = value.toString().length();
			} else if (value instanceof Collection) {
				valueSize = ((Collection) value).size();
			} else {
				return true;
			}
			return valueSize <= validationValue.intValue();
		}
	},
	REGEX("java.lang.String") {
		/*
		 * value should be validated with validation pattern
		 */
		@Override
		public boolean validate(Object value, Object validation) {
//			if(null == value)
//				return true;
			boolean valid = Pattern.matches(validation.toString(), value.toString());
			return valid;
		}
	},
	MIN_DATE("java.time.LocalDate") {
		// value should be greater than or equal to validation date
		@Override
		public boolean validate(Object value, Object validation) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_FORMAT);
			LocalDate valueDate = LocalDate.parse(value.toString(), formatter);
			LocalDate validationDate = LocalDate.parse(validation.toString(), formatter);
			boolean notValid = valueDate.isBefore(validationDate);
			return !notValid;
		}
	},
	MAX_DATE("java.time.LocalDate") {
		@Override
		public boolean validate(Object value, Object validation) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_FORMAT);
			LocalDate valueDate = LocalDate.parse(value.toString(), formatter);
			LocalDate validationDate = LocalDate.parse(validation.toString(), formatter);
			boolean notValid = valueDate.isAfter(validationDate);
			return !notValid;
		}
	},
	MIN_DATETIME("java.time.LocalDateTime") {
		@Override
		public boolean validate(Object value, Object validation) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT);
			LocalDateTime valueDate = LocalDateTime.parse(value.toString(), formatter);
			LocalDateTime validationDate = LocalDateTime.parse(validation.toString(), formatter);
			boolean notValid = valueDate.isBefore(validationDate);
			return !notValid;
		}
	},
	MAX_DATETIME("java.time.LocalDateTime") {
		@Override
		public boolean validate(Object value, Object validation) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT);
			LocalDateTime valueDate = LocalDateTime.parse(value.toString(), formatter);
			LocalDateTime validationDate = LocalDateTime.parse(validation.toString(), formatter);
			boolean notValid = valueDate.isBefore(validationDate);
			return !notValid;
		}
	},
	MIN_NUMBER("java.lang.Double") {
		@Override
		public boolean validate(Object value, Object validation) {
			Double convValidation = Double.valueOf(validation.toString());
			Double convValue = null;
			if (value instanceof Double) {
				convValue = (Double) value;
			} else if (value instanceof Integer) {
				convValue = Double.parseDouble(value.toString());
			}
			if (convValue.compareTo(convValidation) < 0) {
				return false;
			}
			return true;
		}
	},
	MAX_NUMBER("java.lang.Double") {
		@Override
		public boolean validate(Object value, Object validation) {
			Double convValidation = Double.valueOf(validation.toString());
			Double convValue = null;
			if (value instanceof Double) {
				convValue = (Double) value;
			} else if (value instanceof Integer) {
				convValue = Double.parseDouble(value.toString());
			}
			if (convValue.compareTo(convValidation) > 0) {
				return false;
			}
			return true;
		}
	},
	IS_FLOATING_POINT("java.lang.Boolean") {
		@Override
		public boolean validate(Object value, Object validation) {
			Boolean convBoolean = Boolean.valueOf(validation.toString());
			if (value.toString().contains("\\.") == convBoolean.booleanValue()) {
				return true;
			}
			return false;
		}
	},
	IS_MULTI_SELECTED("java.lang.Boolean"), VALUES("java.util.List"), DEFAULT_SELECTED("java.lang.String"),
	IS_MANDATORY("java.lang.Boolean") {
		@Override
		public boolean validate(Object value, Object validation) {
			boolean validationBool = Boolean.valueOf(validation.toString()).booleanValue();
			if (validationBool && null == value) {
				return false;
			}
			return true;
		}
	};
	private String type;

	private RoleFieldDataTypeValidator(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	/**
	 * 
	 * @param value      -> value to be validated
	 * @param validation -> expected value against which input is to be validated
	 * @return
	 */
	public boolean validator(Object value, Object validation) {
		if (!this.equals(IS_MANDATORY) && (value == null || validation == null)) {
			return true;
		}
		return validate(value, validation);
	}

	public boolean validate(Object value, Object validation) {

		return true;
	}

}