package com.kalakutta.entity.enums;

import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.DEFAULT_SELECTED;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.IS_FLOATING_POINT;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.IS_MANDATORY;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.IS_MULTI_SELECTED;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MAX_DATE;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MAX_DATETIME;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MAX_LENGTH;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MAX_NUMBER;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MIN_DATE;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MIN_DATETIME;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MIN_LENGTH;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.MIN_NUMBER;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.REGEX;
import static com.kalakutta.entity.enums.RoleFieldDataTypeValidator.VALUES;

import java.util.ArrayList;
import java.util.List;

public enum RoleFieldDataType {
	STRING{
		{
			validations.add(MIN_LENGTH);
			validations.add(MAX_LENGTH);
			validations.add(REGEX);
		}
	}, 
	DATE{
		{
			validations.add(MIN_DATE);
			validations.add(MAX_DATE);
		}
	}, NUMBER{
		{
			validations.add(MIN_NUMBER);
			validations.add(MAX_NUMBER);
			validations.add(IS_FLOATING_POINT);
		}
	}, DATETIME{
		{
			{
				validations.add(MIN_DATETIME);
				validations.add(MAX_DATETIME);
			}
		}
	}, DROPDOWN{
		{
			validations.add(IS_MULTI_SELECTED);
			validations.add(VALUES);
			validations.add(DEFAULT_SELECTED);
		}
	};
	
	List<RoleFieldDataTypeValidator> validations = new ArrayList<>();
	
	{
		validations.add(IS_MANDATORY);
	}
	public List<RoleFieldDataTypeValidator> getValidations() {
		return validations;
	}
}
