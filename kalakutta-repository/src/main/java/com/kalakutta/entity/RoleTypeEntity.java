package com.kalakutta.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "role_type")
public class RoleTypeEntity extends AbstractEntity {

	@NotNull(message = "type name can't be null")
	@Indexed(unique = true)
	private String name;

	private String description;

	@DBRef
	private Set<RoleFieldEntity> roleFields = new HashSet<>();

	public RoleTypeEntity() {
		// TODO Auto-generated constructor stub
	}

	public RoleTypeEntity(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRoleFields(Collection<RoleFieldEntity> roleFields) {
		this.roleFields.clear();
		this.roleFields.addAll(roleFields);
	}

	public void addRoleField(RoleFieldEntity roleField) {
		if (this.roleFields.contains(roleField)) {
			this.roleFields.remove(roleField);
		}
		this.roleFields.add(roleField);
	}

	public void addRoleFields(Collection<RoleFieldEntity> roleField) {
		this.roleFields.addAll(roleField);
	}

	public Set<RoleFieldEntity> getRoleFields() {
		return roleFields;
	}

	@Override
	public String toString() {
		return "RoleTypeEntity [id=" + getId() + ", name=" + name + ", description=" + description + ", roleFields="
				+ roleFields + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		RoleTypeEntity other = (RoleTypeEntity) obj;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		return true;
	}

}
