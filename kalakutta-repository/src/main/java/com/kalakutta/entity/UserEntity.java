package com.kalakutta.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.kalakutta.entity.utils.RoleFieldValue;

@Document(collection = "user")
public class UserEntity extends AbstractEntity {
	@DBRef
	private RoleTypeEntity roleType;

	private Set<RoleFieldValue> fields = new HashSet<>();

	public void setFields(Collection<RoleFieldValue> fields) {
		this.fields.clear();
		this.fields.addAll(fields);
	}

	public void addField(RoleFieldValue field) {
		if (this.fields.contains(field))
			this.fields.remove(field);
		this.fields.add(field);
	}

	public Set<RoleFieldValue> getFields() {
		return fields;
	}

	public RoleTypeEntity getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleTypeEntity roleType) {
		this.roleType = roleType;
	}

	@SuppressWarnings("unlikely-arg-type")
	public Object getFieldValue(RoleFieldEntity roleField) {
		if (fields.contains(roleField)) {
			for (RoleFieldValue field : this.fields) {
				if (roleField.equals(field)) {
					return field.getFieldValue();
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "UserEntity [roleType= " + roleType + ", fields= " + fields + ", id= " + getId() + "]";
	}

}
