package com.kalakutta.entity;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.data.mongodb.core.mapping.Document;

import com.kalakutta.entity.enums.RoleFieldDataType;
import com.kalakutta.entity.enums.RoleFieldDataTypeValidator;
import com.kalakutta.entity.utils.RoleFieldValue;

@Document(collection = "role_field")
public class RoleFieldEntity extends AbstractEntity implements Comparable<RoleFieldEntity> {
	private String fieldName;
	private RoleFieldDataType fieldType;
	private Map<RoleFieldDataTypeValidator, Object> validators = new HashMap<>();

	public RoleFieldEntity(String fieldName, RoleFieldDataType fieldType) {
		super();
		this.fieldName = fieldName;
		this.fieldType = fieldType;
	}

	public RoleFieldEntity() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		RoleFieldEntity other;
		if (obj instanceof RoleFieldEntity) {
			 other = (RoleFieldEntity) obj;
		} else if(obj instanceof RoleFieldValue){
			 other = ((RoleFieldValue) obj).getFieldType();
		} else {
			return false;
		}
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

	@Override
	public int compareTo(RoleFieldEntity o) {
		return fieldName.compareTo(o.fieldName);
	}

	@Override
	public String toString() {
		return "RoleFieldEntity [id=" + getId() + ", fieldName= " + fieldName + ", fieldValue= " + fieldType
				+ ", validators= " + validators + "]";
	}

	public String getFieldName() {
		return fieldName;
	}

	public RoleFieldDataType getFieldType() {
		return fieldType;
	}

	public void setValidators(Map<RoleFieldDataTypeValidator, Object> validators) {
		this.validators.clear();
		this.validators.putAll(validators);
	}

	public void addValidator(RoleFieldDataTypeValidator validator, Object value) {
		this.validators.put(validator, value);
	}

	public Map<RoleFieldDataTypeValidator, Object> getValidators() {
		return validators;
	}

	public boolean validate(Object value) throws Exception {
		Set<RoleFieldDataTypeValidator> validators = getValidators().keySet();
		
		for (RoleFieldDataTypeValidator validator : validators) {
			Object validatorValue=getValidators().get(validator);
			boolean valid = validator.validator(value, validatorValue);
			if (!valid) {
				throw new Exception(
						MessageFormat.format("Could not validate property {0} with value {1} for validator {2} with validator value {3}",
								fieldName, value, validator, validatorValue));
			}
		}
		return true;
	}
}