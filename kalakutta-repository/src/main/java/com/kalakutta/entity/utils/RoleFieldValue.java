package com.kalakutta.entity.utils;

import org.springframework.data.mongodb.core.mapping.DBRef;

import com.kalakutta.entity.RoleFieldEntity;

public class RoleFieldValue{
	@DBRef
	private RoleFieldEntity fieldType;
	private Object fieldValue;

	public RoleFieldEntity getFieldType() {
		return fieldType;
	}

	public void setFieldType(RoleFieldEntity fieldType) {
		this.fieldType = fieldType;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}

	@Override
	public String toString() {
		return "RoleFieldValue [fieldType= " + fieldType + ", fieldValue= " + fieldValue + "]";
	}

	@Override
	public int hashCode() {
		return fieldType == null ? 0 : fieldType.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (obj instanceof RoleFieldValue) {
			RoleFieldValue other = (RoleFieldValue) obj;
			if (fieldType == null) {
				if (other.fieldType != null)
					return false;
			} else if (!fieldType.equals(other.fieldType))
				return false;
			return true;
		} else {
			RoleFieldEntity other = (RoleFieldEntity) obj;
			if (fieldType == null) {
				if (other != null)
					return false;
			} else if (!fieldType.equals(other))
				return false;
			return true;
		}
	}

}
