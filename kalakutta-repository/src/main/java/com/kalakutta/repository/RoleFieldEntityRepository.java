package com.kalakutta.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.kalakutta.entity.RoleFieldEntity;

@Repository
public interface RoleFieldEntityRepository extends MongoRepository<RoleFieldEntity, String>{

}
