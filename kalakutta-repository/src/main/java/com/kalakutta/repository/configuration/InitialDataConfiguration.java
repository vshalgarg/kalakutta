package com.kalakutta.repository.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;

@Configuration
class InitialDataConfiguration {

	@Bean
	public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {

		Resource sourceData = new ClassPathResource("initial-data.json");

		Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
		// Set a custom ObjectMapper if Jackson customization is needed

		factory.setResources(new Resource[] {});

		return factory;
	}
}