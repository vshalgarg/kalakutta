package com.kalakutta.repository.main;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration	
@EntityScan(basePackages= {"com.kalakutta.entity"})
@EnableMongoRepositories(basePackages= {"com.kalakutta.repository"})
@PropertySource("classpath:application-repository.properties")
@ComponentScan(basePackages= {"com.kalakutta.repository.configuration","com.kalakutta.common.configuration"})
@EnableMongoAuditing
public class MainRepository {
	
	@Bean
	public ValidatingMongoEventListener validatingMongoEventListener() {
	    return new ValidatingMongoEventListener(validator());
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
	    return new LocalValidatorFactoryBean();
	}
	
}
