package com.kalakutta.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.kalakutta.entity.RoleTypeEntity;

@Repository
public interface RoleTypeEntityRepository extends MongoRepository<RoleTypeEntity, String>{
	RoleTypeEntity findByName(String name);

	RoleTypeEntity findOneById(String roleConfigurationId);
}
