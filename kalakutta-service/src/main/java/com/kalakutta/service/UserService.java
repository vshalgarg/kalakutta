package com.kalakutta.service;

import com.kalakutta.service.dto.UserDTO;
import com.kalakutta.service.exception.ServiceException;

public interface UserService {
	UserDTO createUser(UserDTO dto) throws ServiceException;
}
