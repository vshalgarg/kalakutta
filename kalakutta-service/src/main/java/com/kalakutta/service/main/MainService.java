package com.kalakutta.service.main;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Hello world!
 *
 */

@Configuration
@ComponentScan(basePackages= {"com.kalakutta.service.impl"})
public class MainService 
{
}
