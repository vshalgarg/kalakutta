package com.kalakutta.service.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kalakutta.entity.RoleFieldEntity;
import com.kalakutta.entity.enums.RoleFieldDataType;
import com.kalakutta.entity.enums.RoleFieldDataTypeValidator;

public class RoleFieldDTO implements Comparable<RoleFieldDTO> {
	private String id;
	private String fieldName;
	private RoleFieldDataType fieldType;
	private Map<RoleFieldDataTypeValidator, Object> validators = new HashMap<>();

	public RoleFieldDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public RoleFieldDataType getFieldType() {
		return fieldType;
	}

	public void setFieldType(RoleFieldDataType fieldType) {
		this.fieldType = fieldType;
	}

	public Map<RoleFieldDataTypeValidator, Object> getValidators() {
		return validators;
	}

	public void setValidators(Map<RoleFieldDataTypeValidator, Object> validators) {
		this.validators.clear();
		this.validators.putAll(validators);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "RoleFieldDTO [id=" + id + ", fieldName=" + fieldName + ", fieldType=" + fieldType + ", validators="
				+ validators + "]";
	}

	@Override
	public int compareTo(RoleFieldDTO o) {
		if (o == null) {
			return 1;
		}
		return this.getFieldName().compareTo(o.getFieldName());
	}

	public static RoleFieldDTO toDTO(RoleFieldEntity entity) {
		RoleFieldDTO dto = new RoleFieldDTO();
		dto.setFieldName(entity.getFieldName());
		dto.setFieldType(entity.getFieldType());
		dto.setValidators(entity.getValidators());
		dto.setId(entity.getId());

		return dto;
	}

	public static RoleFieldEntity fromDTO(RoleFieldDTO dto) {
		RoleFieldEntity entity = new RoleFieldEntity(dto.getFieldName(),dto.getFieldType());
		entity.setValidators(dto.getValidators());
		entity.setId(dto.getId());

		return entity;
	}

	public static List<RoleFieldDTO> toDTO(Collection<RoleFieldEntity> entity) {
		List<RoleFieldDTO> dto = new ArrayList<>();

		entity.stream().forEach(e -> dto.add(toDTO(e)));

		return dto;
	}

	public static List<RoleFieldEntity> fromDTO(Collection<RoleFieldDTO> dto) {
		List<RoleFieldEntity> entity = new ArrayList<>();

		dto.stream().forEach(e -> entity.add(fromDTO(e)));

		return entity;
	}

}
