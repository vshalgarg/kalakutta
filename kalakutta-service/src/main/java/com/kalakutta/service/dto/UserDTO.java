package com.kalakutta.service.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.kalakutta.entity.UserEntity;

public class UserDTO {
	private String id;
	private RoleTypeDTO roleType;
	private List<RoleFieldValueDTO> fields = new ArrayList<>();

	public List<RoleFieldValueDTO> getFields() {
		return fields;
	}

	public void setFields(Collection<RoleFieldValueDTO> fields) {
		this.fields.clear();
		this.fields.addAll(fields);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RoleTypeDTO getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleTypeDTO roleType) {
		this.roleType = roleType;
	}

	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", roleType=" + roleType + ", fields=" + fields + "]";
	}

	public static UserDTO toDTO(UserEntity entity) {
		UserDTO dto = new UserDTO();
		dto.setId(entity.getId());
		dto.setFields(RoleFieldValueDTO.toDTO(entity.getFields()));
		dto.setRoleType(RoleTypeDTO.toDTO(entity.getRoleType()));

		return dto;
	}

	public static UserEntity fromDTO(UserDTO dto) {
		UserEntity entity = new UserEntity();
		entity.setId(dto.getId());
		entity.setFields(RoleFieldValueDTO.fromDTO(dto.getFields()));
		entity.setRoleType(RoleTypeDTO.fromDTO(dto.getRoleType()));

		return entity;

	}
}
