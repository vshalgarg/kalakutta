package com.kalakutta.service.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.kalakutta.entity.RoleTypeEntity;

public class RoleTypeDTO implements Comparable<RoleTypeDTO> {
	private String id;

	@NotNull
	private String name;
	private String description;

	private List<RoleFieldDTO> fields = new ArrayList<>();

	public RoleTypeDTO(String name) {
		this.name = name;
	}

	public RoleTypeDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<RoleFieldDTO> getFields() {
		return fields;
	}

	public void setFields(Collection<RoleFieldDTO> fields) {
		this.fields.clear();
		this.fields.addAll(fields);
	}

	@Override
	public String toString() {
		return "RoleTypeDTO [id= " + id + ", name= " + name + ", description= " + description + ", fields= " + fields
				+ "]";
	}

	public static List<RoleTypeDTO> toDTO(Collection<RoleTypeEntity> entities) {
		List<RoleTypeDTO> dto = new ArrayList<>();

		entities.stream().forEach(e -> dto.add(toDTO(e)));

		return dto;
	}

	public static RoleTypeDTO toDTO(RoleTypeEntity entity) {
		RoleTypeDTO dto = new RoleTypeDTO(entity.getName());
		dto.setDescription(entity.getDescription());
		dto.setId(entity.getId());
		dto.setFields(RoleFieldDTO.toDTO(entity.getRoleFields()));
		return dto;
	}

	public static Set<RoleTypeEntity> fromDTO(Collection<RoleTypeDTO> dto) {
		Set<RoleTypeEntity> entities = new HashSet<>();

		dto.stream().forEach(e -> entities.add(fromDTO(e)));

		return entities;
	}

	public static RoleTypeEntity fromDTO(RoleTypeDTO dto) {
		RoleTypeEntity entity = new RoleTypeEntity(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setId(dto.getId());
		entity.setRoleFields(RoleFieldDTO.fromDTO(dto.getFields()));
		return entity;
	}

	@Override
	public int compareTo(RoleTypeDTO o) {
		if (o == null)
			return -1;
		return name.compareTo(o.getName());
	}
}
