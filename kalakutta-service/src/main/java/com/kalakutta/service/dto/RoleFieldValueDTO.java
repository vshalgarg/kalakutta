package com.kalakutta.service.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.kalakutta.entity.utils.RoleFieldValue;

public class RoleFieldValueDTO {
	private RoleFieldDTO fieldType;
	private Object fieldValue;

	public RoleFieldDTO getFieldType() {
		return fieldType;
	}

	public void setFieldType(RoleFieldDTO fieldType) {
		this.fieldType = fieldType;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}

	@Override
	public String toString() {
		return "RoleFieldValueDTO [fieldType=" + fieldType + ", fieldValue=" + fieldValue + "]";
	}

	public static RoleFieldValueDTO toDTO(RoleFieldValue entity) {
		RoleFieldValueDTO dto = new RoleFieldValueDTO();
		dto.setFieldType(RoleFieldDTO.toDTO(entity.getFieldType()));
		dto.setFieldValue(entity.getFieldValue());

		return dto;

	}

	public static List<RoleFieldValueDTO> toDTO(Collection<RoleFieldValue> entity) {
		List<RoleFieldValueDTO> dto = new ArrayList<>();

		entity.forEach(e -> dto.add(toDTO(e)));

		return dto;
	}

	public static RoleFieldValue fromDTO(RoleFieldValueDTO dto) {
		RoleFieldValue entity = new RoleFieldValue();
		entity.setFieldType(RoleFieldDTO.fromDTO(dto.getFieldType()));
		entity.setFieldValue(dto.getFieldValue());

		return entity;

	}

	public static List<RoleFieldValue> fromDTO(Collection<RoleFieldValueDTO> dto) {
		List<RoleFieldValue> entity = new ArrayList<>();

		dto.forEach(e -> entity.add(fromDTO(e)));

		return entity;
	}
}
