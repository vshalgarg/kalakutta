package com.kalakutta.service;

import java.util.List;

import com.kalakutta.service.dto.RoleFieldDTO;
import com.kalakutta.service.dto.RoleTypeDTO;
import com.kalakutta.service.exception.ServiceException;

public interface RoleTypeService {
	RoleTypeDTO create(RoleTypeDTO roleTypeDTO) throws ServiceException;

	RoleTypeDTO getRoleTypeByName(String roleTypename) throws ServiceException;

	RoleTypeDTO getRoleTypeById(String roleTypeId) throws ServiceException;

	RoleTypeDTO addFieldToRole(String roleTypeId, RoleFieldDTO roleFieldDTO) throws ServiceException;

	RoleTypeDTO addFieldsToRole(String roleTypeId, List<RoleFieldDTO> roleFieldsDTO) throws ServiceException;
}
