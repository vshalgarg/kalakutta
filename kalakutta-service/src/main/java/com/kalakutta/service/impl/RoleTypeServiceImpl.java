package com.kalakutta.service.impl;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kalakutta.entity.RoleFieldEntity;
import com.kalakutta.entity.RoleTypeEntity;
import com.kalakutta.repository.RoleTypeEntityRepository;
import com.kalakutta.service.RoleFieldService;
import com.kalakutta.service.RoleTypeService;
import com.kalakutta.service.dto.RoleFieldDTO;
import com.kalakutta.service.dto.RoleTypeDTO;
import com.kalakutta.service.exception.ServiceException;

@Service
public class RoleTypeServiceImpl implements RoleTypeService {

	private RoleTypeEntityRepository roleTypeRepository;

	@Autowired
	private RoleFieldService roleFieldService;

	@Autowired
	public RoleTypeServiceImpl(RoleTypeEntityRepository roleTypeRepository) {
		this.roleTypeRepository = roleTypeRepository;
	}

	/*
	 * creates a new RoleType if not created. RoleType can be created with or
	 * without fields
	 * 
	 * @see com.kalakutta.service.RoleTypeService#create(com.kalakutta.service.dto.
	 * RoleTypeDTO)
	 */
	@Override
	public RoleTypeDTO create(RoleTypeDTO roleTypesDTO) throws ServiceException {
		try {
			RoleTypeEntity entity = RoleTypeDTO.fromDTO(roleTypesDTO);
			if (!roleTypesDTO.getFields().isEmpty()) {
				List<RoleFieldEntity> fieldEntity = roleFieldService.createRoleFieldsEntity(entity.getRoleFields());
				entity.setRoleFields(fieldEntity);
			}
			entity = roleTypeRepository.insert(entity);
			return RoleTypeDTO.toDTO(entity);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

	public RoleTypeDTO add(RoleTypeDTO roleTypeDTO) throws ServiceException {
		RoleTypeEntity entity = RoleTypeDTO.fromDTO(roleTypeDTO);
		try {

			if (getRoleTypeByName(entity.getName()) != null) {
				throw new ServiceException(
						MessageFormat.format("role type with name {0} already exist.", entity.getName()));
			}
			entity = roleTypeRepository.insert(entity);
			return RoleTypeDTO.toDTO(entity);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * get roletype by name
	 * 
	 * @return RoleTypeDTO representing roletype
	 * 
	 * @see
	 * com.kalakutta.service.RoleTypeService#getRoleTypeByName(java.lang.String)
	 */
	@Override
	public RoleTypeDTO getRoleTypeByName(String roleTypeName) throws ServiceException {
		try {
			RoleTypeEntity entity = roleTypeRepository.findByName(roleTypeName);
			if (null == entity) {
				throw new ServiceException("Could not find role type with name " + roleTypeName);
			}
			return RoleTypeDTO.toDTO(entity);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}

	}

	/*
	 * get roletype by id
	 * 
	 * @return RoleTypeDTO representing roletype
	 * 
	 * @see com.kalakutta.service.RoleTypeService#getRoleTypeById(java.lang.String)
	 */
	@Override
	public RoleTypeDTO getRoleTypeById(String roleTypeId) throws ServiceException {
		RoleTypeEntity entity = getRoleTypeEntityById(roleTypeId);
		return RoleTypeDTO.toDTO(entity);
	}

	private RoleTypeEntity getRoleTypeEntityById(String roleTypeId) throws ServiceException {
		try {
			RoleTypeEntity entity = roleTypeRepository.findOneById(roleTypeId);
			if (null == entity) {
				throw new ServiceException("Could not find role configuration with id " + roleTypeId);
			}
			return entity;
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * add a field to role type
	 * 
	 * @see com.kalakutta.service.RoleTypeService#addFieldToRole(java.lang.String,
	 * com.kalakutta.service.dto.RoleTypeDTO)
	 */
	@Override
	public RoleTypeDTO addFieldToRole(String roleTypeId, RoleFieldDTO roleFieldDTO) throws ServiceException {
		RoleTypeEntity entity = getRoleTypeEntityById(roleTypeId);
		RoleFieldEntity fieldEntity = RoleFieldDTO.fromDTO(roleFieldDTO);
		fieldEntity = roleFieldService.createRoleFieldEntity(fieldEntity);

		entity.addRoleField(fieldEntity);
		try {
			entity = roleTypeRepository.save(entity);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return RoleTypeDTO.toDTO(entity);
	}

	/*
	 * add multiple fields to a role
	 * 
	 * @see com.kalakutta.service.RoleTypeService#addFieldsToRole(java.lang.String,
	 * java.util.List)
	 */
	@Override
	public RoleTypeDTO addFieldsToRole(String roleTypeId, List<RoleFieldDTO> roleFieldsDTO) throws ServiceException {
		RoleTypeEntity entity = getRoleTypeEntityById(roleTypeId);
		List<RoleFieldEntity> fieldEntity = RoleFieldDTO.fromDTO(roleFieldsDTO);
		fieldEntity = roleFieldService.createRoleFieldsEntity(fieldEntity);

		entity.addRoleFields(fieldEntity);
		try {
			entity = roleTypeRepository.save(entity);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return RoleTypeDTO.toDTO(entity);
	}
}
