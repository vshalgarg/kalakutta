package com.kalakutta.service.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kalakutta.entity.RoleFieldEntity;
import com.kalakutta.repository.RoleFieldEntityRepository;
import com.kalakutta.service.RoleFieldService;
import com.kalakutta.service.dto.RoleFieldDTO;
import com.kalakutta.service.exception.ServiceException;

@Service
public class RoleFieldServiceImpl implements RoleFieldService {

	private RoleFieldEntityRepository roleFieldsRepository;

	@Autowired
	public RoleFieldServiceImpl(RoleFieldEntityRepository roleFieldsRepository) {
		this.roleFieldsRepository = roleFieldsRepository;
	}

	@Override
	public RoleFieldDTO createRoleField(RoleFieldDTO dto) throws ServiceException {
		RoleFieldEntity entity = RoleFieldDTO.fromDTO(dto);
		entity = createRoleFieldEntity(entity);
		return RoleFieldDTO.toDTO(entity);
	}

	@Override
	public RoleFieldEntity createRoleFieldEntity(RoleFieldEntity entity) throws ServiceException {
		try {
			entity = roleFieldsRepository.insert(entity);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return entity;
	}

	@Override
	public List<RoleFieldEntity> createRoleFieldsEntity(Collection<RoleFieldEntity> entity) throws ServiceException {
		try {
			List<RoleFieldEntity> persistedEntity = roleFieldsRepository.insert(entity);
			return persistedEntity;
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}
}
