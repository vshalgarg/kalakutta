package com.kalakutta.service.impl;

import java.text.MessageFormat;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kalakutta.entity.RoleFieldEntity;
import com.kalakutta.entity.RoleTypeEntity;
import com.kalakutta.entity.UserEntity;
import com.kalakutta.repository.RoleTypeEntityRepository;
import com.kalakutta.repository.UserRepository;
import com.kalakutta.service.UserService;
import com.kalakutta.service.dto.UserDTO;
import com.kalakutta.service.exception.ServiceException;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleTypeEntityRepository roleTypeRepository;

	@Override
	public UserDTO createUser(UserDTO dto) throws ServiceException {
		try {
			UserEntity entity = UserDTO.fromDTO(dto);
			entity = createUserEntity(entity);
			return UserDTO.toDTO(entity);

		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

	private UserEntity createUserEntity(UserEntity entity) throws ServiceException {
		try {
			RoleTypeEntity roleType = roleTypeRepository.findOneById(entity.getRoleType().getId());
			if (roleType == null) {
				throw new ServiceException(
						MessageFormat.format("No role type found with id: {0}", entity.getRoleType().getId()));
			}
			// we will check for validators and role fields for roleType, so that we can
			// validate complete validations for a role
			for (RoleFieldEntity roleField : roleType.getRoleFields()) {
				roleField.validate(entity.getFieldValue(roleField));
			}
			
			entity = userRepository.insert(entity);
			Optional<UserEntity> optionalEntity = userRepository.findById(entity.getId());
			return optionalEntity.get();
			 
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(),e);
		}
	}

}
