package com.kalakutta.service.exception;

public class ServiceException extends Exception{

	private static final long serialVersionUID = 7026256473008721256L;

	public ServiceException() {
		super();
	}
	
	public ServiceException(Throwable e) {
		super(e);
	}
	
	public ServiceException(String msg, Throwable e) {
		super(msg,e);
	}
	
	public ServiceException(String msg) {
		super(msg);
	}
}
