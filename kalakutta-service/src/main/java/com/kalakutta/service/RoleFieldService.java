package com.kalakutta.service;

import java.util.Collection;
import java.util.List;

import com.kalakutta.entity.RoleFieldEntity;
import com.kalakutta.service.dto.RoleFieldDTO;
import com.kalakutta.service.exception.ServiceException;

public interface RoleFieldService {
	RoleFieldDTO createRoleField(RoleFieldDTO entity) throws ServiceException;

	RoleFieldEntity createRoleFieldEntity(RoleFieldEntity dto) throws ServiceException;

	List<RoleFieldEntity> createRoleFieldsEntity(Collection<RoleFieldEntity> roleFieldsDTO) throws ServiceException;
}
